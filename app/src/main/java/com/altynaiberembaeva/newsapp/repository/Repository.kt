package com.altynaiberembaeva.newsapp.repository

import com.altynaiberembaeva.newsapp.model.NewsModel
import com.altynaiberembaeva.newsapp.retrofit.RetrofitInstance

class Repository {

   suspend fun getNewsOneData(): NewsModel =
        RetrofitInstance.api.getNewsOneData()
}