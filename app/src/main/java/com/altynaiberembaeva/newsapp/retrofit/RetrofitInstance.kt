package com.altynaiberembaeva.newsapp.retrofit

import com.altynaiberembaeva.newsapp.BuildConfig
import com.altynaiberembaeva.newsapp.util.Constans.Companion.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    private val retrofit by lazy {
        Retrofit.Builder()
            .client(provideOkHttp())
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun provideOkHttp(): OkHttpClient {
        val client = OkHttpClient().newBuilder()
        if (BuildConfig.DEBUG) {
            client.addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }
        return client.build()
    }

    val api: NewsOneAPI by lazy {
        retrofit.create(NewsOneAPI::class.java)
    }
}