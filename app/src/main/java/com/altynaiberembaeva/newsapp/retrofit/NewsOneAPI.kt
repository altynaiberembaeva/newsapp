package com.altynaiberembaeva.newsapp.retrofit

import com.altynaiberembaeva.newsapp.model.NewsModel
import retrofit2.http.GET

interface NewsOneAPI {

    @GET("v2/everything?q=tesla&from=2021-02-21&sortBy=publishedAt&apiKey=82adbd1387014ec098a44a0fb95c5c5a")
    suspend fun getNewsOneData(): NewsModel
}