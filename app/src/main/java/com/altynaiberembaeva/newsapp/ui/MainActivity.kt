package com.altynaiberembaeva.newsapp.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.altynaiberembaeva.newsapp.R
import com.altynaiberembaeva.newsapp.adapters.Adapter
import com.altynaiberembaeva.newsapp.repository.Repository

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var articlesAdapter: Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rv = findViewById<RecyclerView>(R.id.recycler_view)
        articlesAdapter = Adapter()

        rv.layoutManager = LinearLayoutManager(this)
        rv.adapter = articlesAdapter

        val repository = Repository()
        val viewModelFactory = ViewModelFactory(repository)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
        viewModel.article.observe(this, {
            articlesAdapter.addData(it.articles)
            Log.e("aaaa", "rrrrrr")
//            rv.layoutManager = LinearLayoutManager(this)
        })

    }

    override fun onResume() {
        super.onResume()
        viewModel.getArticles()

    }
}