package com.altynaiberembaeva.newsapp.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.altynaiberembaeva.newsapp.model.NewsModel
import com.altynaiberembaeva.newsapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel (private val repository: Repository) : ViewModel(){

    private val _articles: MutableLiveData<NewsModel> = MutableLiveData()
    val article: LiveData<NewsModel> get() = _articles

    fun getArticles() {
        safeCall {
            _articles.postValue(repository.getNewsOneData())
        }
    }

    private fun safeCall(action: suspend () -> Unit) {
        try {
            viewModelScope.launch(Dispatchers.IO) {
                action()
            }
        } catch (e: Exception) {
            Log.e("ERROR", e.message.toString())
        }
    }
}