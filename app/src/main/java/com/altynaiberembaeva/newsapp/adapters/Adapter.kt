package com.altynaiberembaeva.newsapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.altynaiberembaeva.newsapp.R
import com.altynaiberembaeva.newsapp.model.Article
import com.bumptech.glide.Glide

class Adapter :
    RecyclerView.Adapter<Adapter.ViewHolder>() {

    private var articlesList: List<Article> = ArrayList()

    fun addData(list: List<Article>){
        articlesList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.news_one_card_view, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int = articlesList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(articlesList[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(article: Article) {
            itemView.findViewById<TextView>(R.id.tv_title).text = article.title
            itemView.findViewById<TextView>(R.id.tv_description).text = article.description

            Glide.with(itemView)
                    .load(article.urlToImage)
                    .into(itemView.findViewById(R.id.iv_image))
        }
    }
}